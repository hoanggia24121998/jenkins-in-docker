FROM jenkins/jenkins:lts
USER root
RUN apt update -y
RUN curl -sSL https://get.docker.com/ | sh
USER jenkins